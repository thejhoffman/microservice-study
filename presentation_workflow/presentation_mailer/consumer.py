import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    data = json.loads(body)
    send_mail(
        "Your presentation has been accepted",
        f"{data['presenter_name']}, we're happy to tell you that your"
        f"presentation {data['title']} has been accepted",
        "admin@conference.go",
        [data["presenter_email"]],
        fail_silently=False,
    )


def process_rejection(ch, method, properties, body):
    data = json.loads(body)
    send_mail(
        "Your presentation has been rejected",
        f"{data['presenter_name']}, we're sad to tell you that your"
        f"presentation {data['title']} has been rejected",
        "admin@conference.go",
        [data["presenter_email"]],
        fail_silently=False,
    )


def establish_queue(channel, queue, status):
    channel.queue_declare(queue=queue)
    channel.basic_consume(
        queue=queue,
        on_message_callback=status,
        auto_ack=True,
    )


while True:
    try:
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)

        channel = connection.channel()
        establish_queue(channel, "presentation_approvals", process_approval)
        establish_queue(channel, "presentation_rejections", process_rejection)
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
